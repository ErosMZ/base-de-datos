# Reto 2: Consultas básicas

EROS MUÑOZ ZANÓN.

En este reto trabajamos con la base de datos `empresa` y `videoclub`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/ErosMZ/base-de-datos.git


## QUERY 1

En este ejemplo he usado el `use videoclub` para que seleccione la base de datos de videoclub.

`select * from GENERE;` y `select * from PELICULA;` He impreso por pantalla las dos tablas simplemente para guiarme.

Después he seleccionado las dos columnas que quiero que se impriman que en este caso es la columna __Descripcio y Titulo__.

Seguidamente selecciono la tabla películas con `from PELICULA`
Y por último hago un `join` de las claves principales para que se unan las tablas
```sql
use videoclub;

select * from GENERE;
select * from PELICULA;

select PELICULA.Titol , GENERE.Descripcio
from PELICULA
join GENERE on PELICULA.CodiGenere=GENERE.CodiGenere;
```

## QUERY 2
En este caso he selecionado las dos columnas que quiero imprimir que en este caso son __Nom y CodiFactura__

He seleccionado con el `from` la tabla __Client__.

Con el `join` seleccionado la tabla __Factura__ y he unido las dos columnas que tienen relación.

Y para finalizar he usado el `Where` para que el nombre de la tabla cliente empieze por __MAria__.

El `select * from CLIENT` lo he utilizado simplemente para ubicarme mejor.
```sql
select  CLIENT.Nom , FACTURA.CodiFactura
from  CLIENT
JOIN  FACTURA
ON FACTURA.DNI=CLIENT.DNI
WHERE CLIENT.Nom LIKE "Maria%";

select * from CLIENT
```

## QUERY 3

En este ejemplo he usado el `use videoclub` para que seleccione la base de datos de videoclub.

He hecho un `select` para indicar las columnas que quiero imprimir que en este caso es __Titol , SegunaPart y Nom__

He indicado la tabla __ACTOR__ con el `from`.

Y con el `join` he seleccionado la tabla __Pelicula__  y seguidamente he unido las dos columnas con el `on` que tienen relación entre ellas.

`SELECT * FROM ACTOR` aquí simplemente he impreso la tabla __actor__ para guiarme.
```sql
USE VIDEOCLUB;
SELECT PELICULA.Titol , PELICULA.SegonaPart , ACTOR.Nom
FROM ACTOR
JOIN PELICULA
ON ACTOR.CodiActor = PELICULA.CodiActor;
SELECT * FROM ACTOR
```

## QUERY 4

En este último ejemplo he selecionado con el `select` las dos columnas que quiero que se muestren que en este caso es __Titol y Nom.__

En el segundo paso he seleccionado la tabla __Pelicula__ con el `from`

Con los `joins` he seleccionado las otras dos tablas que en este caso son __Pelicula e interpretada.__

Y por ultimo he hecho las relaciones de con el `on` y el `and` para indicar las siguiente.
```sql
SELECT PELICULA.Titol , ACTOR.Nom
from PELICULA
JOIN INTERPRETADA
JOIN ACTOR
ON PELICULA.CodiPeli = INTERPRETADA.CodiPeli
AND ACTOR.CodiActor = INTERPRETADA.CodiActor
```