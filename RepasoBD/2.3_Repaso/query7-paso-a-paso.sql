-- DURACION TOTOAL DE UN ALBUM
SELECT Milliseconds from Track
where AlbumId = 1;

-- DURACION TOTAL DE TODOS LOS ALBUMNES
SELECT AlbumId , SUM(Milliseconds) as SumaMilisegundos 
from Track
GROUP BY AlbumId -- Ordenado por albumId
having SumaMilisegundos > (
	select avg(DuracionTotal)
    from (
		select SUM(Milliseconds) as DuracionTotal
        from Track
        group by AlbumId	
	) as TablaMedia
);

SELECT AlbumId , Title ,SUM(Milliseconds) as SumaMilisegundos 
from Track
GROUP BY AlbumId -- Ordenado por albumId
having SumaMilisegundos > (
	select avg(DuracionTotal)
    from (
		select SUM(Milliseconds) as DuracionTotal
        from Track
        group by AlbumId	
	) as TablaMedia
)