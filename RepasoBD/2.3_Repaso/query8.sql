select (
	
    select Name from Genre
    where Track.GenreId = Genre.GenreId

) as Genero, count(TrackId) as Canciones 
from Track
group by GenreId
limit 1;

select * 
from Track
where GenreId in (
    select GenreId 
    from Genre 
    where Name = 'Rock'
)

