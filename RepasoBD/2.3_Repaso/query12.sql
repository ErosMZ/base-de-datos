USE Chinook;
DESCRIBE Employee;
DESCRIBE Invoice;
DESCRIBE Customer;

-- TOTAL DE LAS FACTURAS DEL CLIENTE 1
SELECT SUM(Total) FROM Invoice
WHERE CustomerId = 1;

-- CLIENTES DE CADA EMPPLEADO
SELECT CustomerId FROM Customer
WHERE SupportRepId = 3;

-- TOTAL DE LAS FACTURAS DE VARIOS CLIENTE
SELECT SUM(Total) FROM Invoice
WHERE CustomerId IN (1,3,12);

-- TOTAL DE LAS FACTURAS DE UN EMPLEADO
SELECT SUM(Total) FROM Invoice
WHERE CustomerId IN ( 
	SELECT CustomerId FROM Customer
    where SupportRepId = 3
);

-- RESULTADO FINAL
SELECT EmployeeId, LastName, FirstName, (
	SELECT SUM(Total) from Invoice
    where CustomerId in (
		SELECT CustomerId FROM Customer
        WHERE Customer.SupportRepId = Employee.EmployeeId
        )
    )AS TOTALVENTAS
from Employee



