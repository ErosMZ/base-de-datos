-- obtener los albunes con un numero de canciones superiores a la media
select AlbumId , COUNT(*) as n_tracks
from Track
group by AlbumId
having n_tracks > (
	
    Select avg(Num_Canciones)
    from (
        select Track.AlbumId, COUNT(Track.TrackId) as Num_Canciones
		from Track 
        group by AlbumId
    
    ) as Ntrack
    
)



