-- obtener los albunes con una duracion total superior a la media

select * from Album
where AlbumId in (
	
    select AVG(Milliseconds) from Track
    where
		Album.AlbumId=Track.AlbumId

) 