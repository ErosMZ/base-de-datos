-- obtener las canciones con una duración superior a la media
SELECT * FROM Track
where Milliseconds > ( select avg(Milliseconds) 
from Track
)
order by Milliseconds desc	