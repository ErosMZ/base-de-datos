SELECT i.InvoiceDate, c.FirstName, c.LastName, c.Address, c.PostalCode, c.Country, i.Total
FROM Customer AS c
JOIN Invoice AS i ON i.CustomerId = c.CustomerId
WHERE c.City LIKE "Berlin";