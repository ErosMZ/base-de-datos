create database Gestión_Vuelos;
use Gestión_Vuelos;

CREATE TABLE Vuelos (
	idVuelo varchar(5) PRIMARY KEY NOT NULL,
    origen varchar(50) NOT NULL,
    destino varchar(50) NOT NULL,
    fecha_salida date NOT NULL,
    fecha_llegada date NOT NULL,
    modelo varchar(100)
);

CREATE TABLE Pasajeros (
	DNI varchar(9) PRIMARY KEY NOT NULL,
	nombre varchar(10),
    apellidos varchar(40),
	fecha_nacimiento date NOT NULL,
    sexo enum("F", "M") NOT NULL,
    dirección varchar(40) NOT NULL
);
CREATE TABLE Reservas (
	numReserva INT PRIMARY KEY auto_increment,
    DNI varchar(9) NOT NULL,
    idVuelo varchar(5) NOT NULL,
    asiento varchar(3) NOT NULL,
    clase varchar(20) NOT NULL,
    FOREIGN KEY (idVuelo) REFERENCES Vuelos(idVuelo),
    FOREIGN KEY (DNI) REFERENCES Pasajeros(DNI)
);

INSERT INTO Vuelos (idVuelo,  origen, destino, fecha_salida, fecha_llegada, modelo)
VALUES
("E5475", "España", "Canada", "2024-04-12", "2024-04-13", "AirTurbo-34U6M");

INSERT INTO Pasajeros (DNI, nombre, apellidos, fecha_nacimiento, sexo,  dirección)
VALUES
("48598859M", "Eros", "Muñoz Zanón", "2005-06-06", "M", "Av Pakito32 Num 69 Piso 3 Pta 7");

INSERT INTO Reservas (DNI, idVuelo, asiento, clase)
VALUES
("48598859M", "E5475", "A2", "Firts Class");

/* select Vuelos.idVuelo , Reservas.numReserva Reservas.asiento
from Vuelos
join Reservas
on Reservas.idVuelo= Vuelos.idVUelo */

	