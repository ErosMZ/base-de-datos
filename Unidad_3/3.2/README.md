# ACTIVIDAD 3.1

EROS MUÑOZ ZANÓN

Ver los usuarios y sus hosts
select user, host from mysql.user;
+------------------+-----------+
| user             | host      |
+------------------+-----------+
| dbuser           | %         |
| root             | %         |
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| root             | localhost |
+------------------+-----------+
este es un ejemplo que me ha salido
Estos usuarios se ecuentran en la instalacion de mi mySQL cada ordenador tiene su instancia.

_______________

Renombrar un usuario RENAME USER "nombre"@
## MANEJO DE PERMISOS

### MANEJO DEL USUARIO
Ver permisos
show grants;

He creado el usuario prueba  con el host 127.0.0.1 y contraseña dbrootpass   
CREATE USER prueba@127.0.0.1 IDENTIFIED BY "dbrootpass";

select user, HOST from mysql.user;
+------------------+-----------+
| user             | HOST      |
+------------------+-----------+
| dbuser           | %         |
| prueba           | 127.0.0.1 |
| root             | %         |
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| root             | localhost |
+------------------+-----------+
 
Prueba de que se a creado

Aqui he cambiado el host del usuario porque me daba problemas así  he optado que se pueda conectar por cualquier host.    
RENAME USER "prueba"@"127.0.0.1" to "prueba"@"%"

He creado dos usuarios más.
CREATE USER "prueba1"@"%" IDENTIFIED BY "dbrootpass";
CREATE USER "prueba2"@"%" IDENTIFIED BY "dbrootpass";

select user, HOST from mysql.user;
+------------------+-----------+
| user             | HOST      |
+------------------+-----------+
| dbuser           | %         |
| prueba           | %         |
| root             | %         |
| prueba1          | 127.0.0.1 |
| prueba2          | 127.0.0.1 |
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| root             | localhost |
+------------------+-----------+

Muestro las bases de datos que tiene acceso y vemos que solo tiene acceso a las default.
SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| performance_schema |
+--------------------+


## MANEJO DE PERMISOS
Continuando con el apartado anterior voy hacer que al usuario prueba pueda manejar la database Chinook.

Aqui el comando:   
GRANT SELECT ON Chinook.* TO 'prueba'@'%';

Me deja hacer cualquier cosas con sus tablas ya que he puesto el .* que esto se refiere a todas las tablas de la database.

Confirmar cambios:   
FLUSH PRIVILEGES;
```sql
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| Chinook            |
| information_schema |
| performance_schema |
+--------------------+
```

Me deja hacer selects, updates, inserts etc de sus tablas ya que he puesto el .* que esto se refiere a todas las tablas de la database.
__Eliminacion de usuarios:__
- DROP USER 'prueba'@'%';   
- DROP USER 'prueba1'@'%';    
- DROP USER 'prueba2'@'%'; 
```sql
mysql> select user, HOST from mysql.user;

+------------------+-----------+
| user             | HOST      |
+------------------+-----------+
| dbuser           | %         |
| root             | %         |
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| root             | localhost |
+------------------+-----------+
```
## MANEJO DE ROLES

### PARTE 1 - CREACION DE USUARIOS Y ROLES
En este ejemplo quiero crear usuarios jefe que pueden ver toda base de datos de Chinook
Tambien crearé otros usuarios que sean clientes que pueden ver las tablas de compras y los trabajadores posibles que hay que en este caso son: employee, Track y Album

Los trabajadores verán los clientes que hay y tambien veran compras que hacen que en este caso son las tablas: Invoice, InvoiceLine y Customer


__Creación de los usuarios:__
CREATE USER "Jefe1"@"%" IDENTIFIED BY "dbrootpass";
CREATE USER "Jefe2"@"%" IDENTIFIED BY "dbrootpass";
CREATE USER "Jefe3"@"%" IDENTIFIED BY "dbrootpass";

CREATE USER "trabajador"@"%" IDENTIFIED BY "dbrootpass";
CREATE USER "trabajador1"@"%" IDENTIFIED BY "dbrootpass";
CREATE USER "tranajador2"@"%" IDENTIFIED BY "dbrootpass";

CREATE USER "cliente1"@"%" IDENTIFIED BY "dbrootpass";
CREATE USER "cliente2"@"%" IDENTIFIED BY "dbrootpass";
CREATE USER "cliente3"@"%" IDENTIFIED BY "dbrootpass";


__ROL DE LOS JEFES__
CREATE ROLE Jefes;
GRANT SELECT ON Chinook.* to Jefes;

GRANT "Jefes" to Jefe1;
GRANT "Jefes" to Jefe2;
GRANT "Jefes" to Jefe3;


__Rol de los Trabajadores__
CREATE ROLE Trabajadores;
GRANT SELECT ON Chinook.Invoice to Trabajadores;
GRANT SELECT ON Chinook.InvoiceLine to Trabajadores;
GRANT SELECT ON Chinook.Customer to Trabajadores;

GRANT "Trabajadores" to trabajador;
GRANT "Trabajadores" to trabajador1;
GRANT "Trabajadores" to trabajador2;

__ROL DE LOS CLIENTES__
CREATE ROLE Clientes;   
GRANT SELECT ON Chinook.Employee to Clientes;   
GRANT SELECT ON Chinook.Track to Clientes;   
GRANT SELECT ON Chinook.Album to Clientes;   

GRANT "Clientes" to cliente1;
GRANT "Clientes" to cliente2;
GRANT "Clientes" to cliente3;
```sql
select user, host from mysql.user;
+------------------+-----------+
| user             | host      |
+------------------+-----------+
| Clientes         | %         |
| Jefe1            | %         |
| Jefe2            | %         |
| Jefe3            | %         |
| Jefes            | %         |
| Trabajadores     | %         |
| cliente1         | %         |
| cliente2         | %         |
| cliente3         | %         |
| dbuser           | %         |
| root             | %         |
| trabajador       | %         |
| trabajador1      | %         |
| trabajador2      | %         |
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| root             | localhost |
+------------------+-----------+
```

### PARTE 2 - DIFERENTES TIPOS DE CAMBIOS DE ROLES

El trabajador2 trabaja muy bien asi que han decidido ascenderlo a jefe asi que hay que cambiarle el rol que tiene de Trabajador a el de jefe.
Pero claro en la empresa quieren tener solamente 3 jefes asi que van a degradar al jefe3 a trabajador.
Y además tendré que cambiarles el nombre ya que dejan de ser trabajador o jefe.


__Ascender al trabajador2__
REVOKE Trabajadores from trabajador2;
GRANT Jefes to trabajador2;

__Degradar al jefe3__
REVOKE Jefes FROM Jefe2;
GRANT Trabajadores TO Jefe2;

__Cambio de nombres__
RENAME USER 'Jefe2'@'%' TO 'NuevoTrabajador'@'%';
RENAME USER 'trabajador2'@'%' TO 'NuevoJefe'@'%';

## APUNTES
__que es una clave foranea__

__desde que ordenadores puede entrar un usuario__

 __diferencia entre privilegios estatáticos y dinamicos__
__CAMBIAR ROL__
REVOKE Jefes FROM Jefe2;
GRANT Trabajadores TO Jefe2;

-------------------------------------------
__PONER UN ROL AL USUARIO CON EL QUE ESTOY__
SET ROLE Jefes;

SELECT CURRENT_ROLE()// VER LOS ROLES QUE TENGO AHORA

SET ROLE "rol"; // PONERSE UN ROL

GRANT "rol" TO "usuario"; // dar a un usuario un rol

 SHOW GRANTS FOR 'nombre_usuario'@'localhost';  // mostrar los roles de un usuario
 SHOW GRANTS;