SELECT 
	count(*) as total,
	AVG(Milliseconds)/1000 as "Media (s)",
    MAX(Milliseconds)/1000/60 as "Max (min)"
from Track;

SELECT 
	COMPOSER,
	AVG(Milliseconds)/1000 as "Media (s)"
from Track
group by Composer
ORDER BY AVG(Milliseconds);
