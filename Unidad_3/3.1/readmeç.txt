# ACTIVIDAD 3.1

EROS MUÑOZ ZANÓN

En este reto trabajamos con __mysql CLI__, también tratamos con la creacion de bases de datos junto a sus tablas y columnas.

Todo subido en https://gitlab.com/ErosMZ/base-de-datos.git

 ## CONECTARSE AL MYSQL CLI
mysql -u *"usuario"* -p -h *"ip host"* -P *"puerto"*

En -p puedes poner la contraseña o si no la pones te la pedirá después de ejecutar el comando

En mi caso es: mysql -u  root -p  -h 127.0.0.1 -P 33006

### COMMIT Y ROLLBACK ¿Para que sirve? ¿Que posibilidades tenemos?

__Commit:__ Se utilza básicamente

__MOSTRAR LAS BASES DE DATOS QUE TIENES__
```sql
SHOW DATABASES;
```

__MOSTRAR LAS TABLAS DE UNA BASE DE DATOS CON DETALLES__
```sql
SHOW FULL TABLES FROM ERMUZA;
```
__MOSTRAR LAS TABLAS DE UNA BASE DE DATOS SIN DETALLES__ 
```sql
SHOW TABLES FROM ERMUZA;
```

__MOSTRAR LAS TABLAS QUE TIENE UNA BASE DE DATOS__
```sql
USE "nombre_bd"; show tables;
```
El USE es simplemente para acceder a la base de datos que quieres ver las tablas

__MOSTRAR LAS COLUMNAS DE UNA TABLA__

En este caso es la bd de datos de ERMUZA y la tabla Mesas
```sql
Show colums from ERMUZA.Mesas;

-- TAMBIÉN SE PUEDE UTULIZAR ESTE OTRO COMANDO
DESCRIBE ERMUZA.Mesas;
```

__MOSTRAR EN QUE BASE DE DATOS ESTOY__

```sql
SELECT DATABASE();
```
__MOSTRAR EL ESTADO DEL COMMIT__
```sql
select @@autocommit;
```
Si pone un 1 esta en marcha el autocommit si pone un 0 no está en marcha

__ACTIVAR COMMITS MANUALES__
```sql
set autocimmit=0;
```
__ACTIVAR AUTOCOMMITS__
```sql
set autocimmit=1;
```
__REALIZAR UN COMMIT__
```sql
COMMIT;
```
__REGRESAR AL ÚLTIMO COMMIT__
```sql
ROLLBACK;
```
Solamente puedes ir al ultimo commit que has hecho


## TRATO CON COMMITS Y ROLLBACK

1- Pongo el autocomit a 0  para que no haga los commits automaticamente 
```sql
set autocimmit=0; 
```
2- Inserto datos en una tabla
```sql
INSERT INTO empresa.COMANDA VALUES(630, CURRENT_DATE(), "A", 100,"1987-01-23",23940.00);
```
3- Hago el commit
```sql
commit;
```
4- Inserto otra linea de datos
```sql
INSERT INTO empresa.COMANDA VALUES(631, CURRENT_DATE(), "A", 100,"1987-01-23",23940.00);
```
5 - Hago un select y me aparacen los dos insert que he hecho anteriormente
```sql
select * from COMANDA;
```

6-  Hago un __ROLLBACK__ para volver al ultimo commit
```sql
ROLLBACK;
```
7-  Muestro la tabla y el ultimo insert que he hecho no aparace ya que hice el commit antes de añadir el ultimo select
```sql
select * from COMANDA
```

## CREACION ESTRUCTURA DE UNA BASE DE DATOS

__CREACIÓN DE LA DATABASE__
```sql
CREATE database Rallané;
USE Rallané;
```
__CREACIÓN DE LA TABLA VUELOS__
```sql
    CREATE TABLE `Rallané`.`vuelos` (
  `idVuelos` INT NOT NULL,
  `Num_Asientos` INT NULL,
  `Origen` VARCHAR(45) NOT NULL,
  `Destino` VARCHAR(45) NOT NULL,
  `Aerolínea` VARCHAR(45) NOT NULL,
  `Salida` DATE NOT NULL,
  `KM` INT NULL,
  PRIMARY KEY (`idVuelos`),
  UNIQUE  (`idVuelos` ASC) VISIBLE);
```
__CREACIÓN TABLA PASAJEROS__
```sql
CREATE TABLE `Rallané`.`vuelos` (
  `idVuelos` INT NOT NULL,
  `Num_Asientos` INT NULL,
  `Origen` VARCHAR(45) NOT NULL,
  `Destino` VARCHAR(45) NOT NULL,
  `Aerolínea` VARCHAR(45) NOT NULL,
  `Salida` DATE NOT NULL,
  `KM` INT NULL,
  PRIMARY KEY (`idVuelos`),
  UNIQUE (`idVuelos` ASC) VISIBLE);
  ```
  
__CREACIÓN TABLA RESERVAS__
```sql
CREATE TABLE `Rallané`.`reservas` (
  `idreservas` INT NOT NULL,
  `idPasajero` INT NOT NULL,
  `idVuelo` INT NOT NULL,
  `fechaReserva` DATE NOT NULL,
  `asiento` VARCHAR(45) NOT NULL,
  `comentarios` VARCHAR(150) NULL,
  PRIMARY KEY (`idreservas`),
  CONSTRAINT `idPasajero`
    FOREIGN KEY (`idPasajero`)
    REFERENCES `Rallané`.`pasajeros` (`idPasajero`),
  CONSTRAINT `idVuelo`
    FOREIGN KEY (`idVuelo`)
    REFERENCES `Rallané`.`vuelos` (`idVuelos`)
);
```
### porque hay tres tablas
El objetivo principal de que hayan tres tablas es para hacer una relación de muchos a muchos.
### TIPOS DE DATOS

`INT` Son para los datos númericos

`NOT NULL` Indica que un campo no puede tener valores nulos, debe contener siempre un valor.

`NULL`  Permite que un campo pueda tener valores nulos, no es obligatorio tener un valor // también se puede indicar que sea nulo sin poner nada a la columna al crearla.

`VARCHAR()` Se utiliza para almacenar cadenas de texto pueden haber letras y números. En le paréntesis indicas el tamaño de caracteres (no se puede dejar en blanco)

`PRIMARY KEY` Identifica de forma única cada registro en una tabla.

`UNIQUE` Harca que los valores en un campo sean únicos en la tabla.

`DATE` Se utiliza para almacenar fechas en formato YYYY-MM-DD.
### CLAVES FORANEAS Y QUE ÉS
Una clave foránea es un campo en una tabla que hace referencia a la clave primaria de otra tabla. Y hace una conexión entre las dos tablas que has indicado y pudiendo relacionarlas posteriormente con por ejemplo `select`.

Se ha definido dos claves foráneas para mantener la integridad de la base de datos. La clave `idPasajero` hace referencia al campo `idPasajero` en la tabla __pasajeros__, mientras que la clave `idVuelo` hace referencia al campo `idVuelos` en la tabla __vuelos__.
añademe aqui una muy breve descripcion de que trata una clave foranea
### CAMPOS NECESARIOS
En la tabla vuelos: `idVuelos`, `Num_Asientos`, `Origen`, `Destino`, `Aerolínea`, `Salida`, `KM`.
En la tabla pasajeros: `idPasajero`, `Nombre`, `PrimerApellido`, `SegundoApellido`, `DNI`, `Nacionalidad`, `FechaNac`, `Genero`.
En la tabla reservas: `idreservas`, `idPasajero`, `idVuelo`, `fechaReserva`, `asiento`, `comentarios`.
### RESTRICCIONES QUE USO Y PORQUE

__PRIMARY KEY:__ Define el campo como clave primaria, garantizando unicidad y permitiendo identificar de forma única cada registro en la tabla.

__FOREIGN KEY:__ Establece una relación entre tablas, asegurando que los valores en el campo de la clave foránea existan en la tabla referenciada.

__UNIQUE:__ Garantiza que los valores en el campo sean únicos en la tabla.

__NOT NULL:__ Indica que el campo no puede contener valores nulos, asegurando la integridad de los datos.