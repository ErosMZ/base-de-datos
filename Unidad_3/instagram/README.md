# RETO INSTAGRAM

EROS MUÑOZ ZANÓN

En este reto trabajamos con la base de datos `instagram_low_cost`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/ErosMZ/base-de-datos.git

## QUERY 1

En esta query he hecho un select muy simple que indica que en __idUsuario__ busque el numero 36 que en este caso es el ID del usuario

```sql
select * from fotos
where
idUsuario like 36
```

## QUERY 2
En este caso me ha dado por cambiar el año para que saliera algo al hacer en el select

Bueno he indicado que en la tabla __Fotos__ en __idUsuario__ busque el id 36.

Y despues le he puesto un `AND` para poner que la __fechaCreacion__ este entre las dos fechas y horas indicadas.

```sql
SELECT * FROM fotos
WHERE idUsuario LIKE '36'
AND fechaCreacion BETWEEN '2024-01-01 00:00:00' AND '2024-01-31 23:59:59';
```

## QUERY 3
En este tercer caso he indicado en el `select` lo que quiero que salga por pantalla.

Con los `join` he juntado las tablas __comentariosFotos__ y __fotos__.

En los `on` he indicado las __FOREIGN KEY__ de cada tabla

Y en el `where` he indicado que en __idUsuario__ de la tabla __comentarios__ busque el usuario 36.
Y en la tabla __fotos__ y columna __idFoto__ busque el id 12.

```sql
select comentarios.comentario  , comentarios.idComentario , comentarios.url
from comentarios
join comentariosFotos 
	on comentarios.idComentario = comentariosFotos.idComentario
join fotos 
	on comentariosFotos.idFoto = fotos.idFoto
where 
	comentarios.idUsuario = 36
	and fotos.idFoto = 12

```

## QUERY 4
En este caso he cambiado el usuario 25 por el 11 para que saliera algo por pantalla

En el `select` indico las columnas que quiero que salgan por pantalla

Hago un `join` de la tabla __reaccionesFotos__.

En el on indico las __FOREIGN KEY__ de las dos tablas.

Y en el `where` simplemente le he indicado que en la tabla __reaccionesFotos__ y la columna __idTipoReaccion__ haya un 4 y en la columna idUsuario haya un 11.

```sql
select fotos.idFoto , fotos.descripcion  from fotos
join reaccionesFotos
on fotos.idFoto=reaccionesFotos.idFoto
where 
	reaccionesFotos.idTipoReaccion like 4
    and reaccionesFotos.idUsuario like 11
```

## QUERY 5

Aqui he hecho un simple intento pero me ha sido muy complicado :(
```sql
select usuarios.nombre from usuarios
join reaccionesfotos
join tiposreaccion
on reaccionesfotos.idTipoReaccion=tiposreaccion.idTipoReaccion
and usuarios.idUsuario=reaccionesfotos.idUsuarios
where usuarios.idRol like 4
and reaccionesfotos.idTipoReaccion like 1
```
## QUERY 6
En este penultimo ejemplo que hecho un select junyo a un COUNT(*) y este ultimo se encarga de contar todo gracias al asterisco de que hay dentro de los "()". Le he puesto un `as` para que al salir por pantalla salga el nombre que he puesto que en este caso es  *Me_divierte*.

Con el `from` he indicado la tabla.

Y para terminar en los `where` he puesto que en __idFoto__ sea igual a 12 y en __idUsuario__ sea 45.
```sql
select count(*) as Me_divierte from reaccionesfotos
where idFoto like 12
and idUsuario like 45
```

## QUERY 7
En último caso es muy parecido al último, así que le voy a explicar al `where` que indíco con los *%* que haya lo que sea delante y detrás y en el medio pongo __playa__ y así cuenta todas las fotos que en la descripción este la palabra __playa__.

```sql
select COUNT(*) as totFotos from  fotos
where
	fotos.descripcion like "%playa%"
```