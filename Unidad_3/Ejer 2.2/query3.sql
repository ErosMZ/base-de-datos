-- query 3
select  *  from Employee
where EmployeeId in (
	select  distinct SupportRepId
	from Customer
    where SupportRepId is not null
)