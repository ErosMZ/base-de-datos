# RETO 2.2

EROS MUÑOZ ZANÓN

En este reto trabajamos con la base de datos `Chinook`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/ErosMZ/base-de-datos.git

## QUERY 2
Uso el `DISTINCT` para eliminar los valores repetidos de la columna __Name__. Y le pongo a la columna el nombre __Nombre de Artita__.

Los `AS` que utilizo para las tablas utilizo para simplifacar los nombres.

Después en el primer `where` cojo el  __ArtistId__ de la tabla __Artist__

En el `select` cojo el __ArtistId__ de la tabla __Album__ para unirlas

Después de album cojo el __idAlbum__ ya que en la tabla __track__ tambien está

Y para terminar hago el `where` de __Milliseconds__ y indicandole __300000__ que equivale a 5 minutos.

```sql
SELECT DISTINCT ar.Name AS 'Nombre de Artista'
FROM Artist AS ar
WHERE ar.ArtistId IN (
    SELECT al.ArtistId
    FROM Album AS al
    WHERE al.AlbumId IN (
        SELECT tr.AlbumId
        FROM Track AS tr
        WHERE tr.Milliseconds > 300000 
    )
);
```
## QUERY 3
En esta query selecciono primero toda la tabla __Employee__.

En la subconsulta selecciono el __EmployeeId__ y dentro seleciono sin que se repitan los valores el la columna __SupportyRepId__ de la tabla __Customer__

Y en el `where` pongo que si __SupportRepId__ no es nulo.

```sql
select  *  from Employee
where EmployeeId in (
	select  distinct SupportRepId
	from Customer
    where SupportRepId is not null
)
```

## Query 4
En esta query he seleccionado la columna __Name__ y __Composer__ de la tabla __Track__

En la subconsulta selecciono __TrackId__ y le pongo que no este con el `not in` y selecciono el __TrackId__ de la tabla __InvoiceLine__
```sql
SELECT Name , Composer
FROM Track
WHERE TrackId NOT IN (
    SELECT TrackId
    FROM InvoiceLine
)
```
