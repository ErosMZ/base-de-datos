SELECT DISTINCT ar.Name AS 'Nombre de Artista'
FROM Artist AS ar
WHERE ar.ArtistId IN (
    SELECT al.ArtistId
    FROM Album AS al
    WHERE al.AlbumId IN (
        SELECT tr.AlbumId
        FROM Track AS tr
        WHERE tr.Milliseconds > 300000 
    )
);