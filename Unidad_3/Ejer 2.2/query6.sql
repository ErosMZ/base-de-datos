select * from Track
where TrackId in (
    select TrackId
    from InvoiceLine
	where InvoiceId in (
        select InvoiceId
        from Invoice
        where CustomerId in (
            select CustomerId from Customer
            where Customer.FirstName = "Luis"
            and Customer.LastName = "Rojas"
        )
    )
);
-- Otra manera de hacerlo
/*select * from Track
where TrackId in (
	select TrackId from InvoiceLine
    where InvoiceId in (
        select InvoiceId From Invoice
        Where CustomerId in (
			Select CustomerId from Customer
            where FirstName = "Luis"
			and LastName = "Rojas"
        )
    
    )
)